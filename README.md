# Rust worker template

This repository is the official template to create a MCAI Rust worker.

Some adaptations should be made when developing your own worker (such as naming or CI configuration), however the project structure and packaging should follow the given guidelines.

## General guidelines

Developing a worker implies that you follow some guidelines, otherwise it might not be able to operate correctly with MCAI's architecture:

- Edit the [`Cargo.toml`](Cargo.toml) file and replace:
  - `name`: declare the name of the worker.
  - `version`: manage the version of the worker.
  - `license`: select the license of the worker (can be Private, Commercial or any [SPDX license](https://spdx.org/licenses/)).
  - `description`: redact a short description of what the worker does.
  - Any other fields supported by `Cargo.toml` can be added too.
  - To use the media SDK, enable the media feature `mcai_worker_sdk = {version="2.0.0-rc4", features = ["media"]}` and follow instructions from the step 3 to adapt the [worker.rs](src/worker.rs) file.


- Edit the [worker.rs](src/worker.rs)
  - Update McaiWorkerParameters class and adapt input parameters of the worker.
  - Implement McaiWorker trait on McaiRustWorker struct as described in the docs.


Configure the Continuous Integration (CI) file to build the Docker image of the worker if necessary.

*We recommend using [`cargo-release`](https://github.com/crate-ci/cargo-release) to manage worker's version.*

## Set up the development environment

Edit the code and start your worker with:

```bash
cargo run
```

You can set a source order (*i.e.* a dummy job that will be executed by the worker) by setting the environment variable `SOURCE_ORDERS` to the path of the source order file.


## Install and run the worker

Please refer to the [SDK documentation](https://docs.rs/mcai_worker_sdk/latest/mcai_worker_sdk/#runtime-configuration) to get information about runtime configuration.

### Locally

```bash
cargo install --path .
rs_mcai_worker
```

The command line name (`rs_mcai_worker`) depends on the value (right parameter) you gave in the [Cargo.toml `bin` section](Cargo.toml#L12).

### Docker

```bash
docker build -t rs_mcai_worker .
docker run --rm rs_mcai_worker
```

You may have to configure additional flags in the `docker run` command line, such as environment variables for connecting to RabbitMQ or source orders.
