use mcai_worker_sdk::{default_rust_mcai_worker_description, prelude::*};
use serde::Deserialize;

#[derive(Debug, Deserialize, JsonSchema)]
struct McaiWorkerParameters {
  #[allow(dead_code)]
  source_path: Option<String>,
  #[allow(dead_code)]
  destination_path: Option<String>,
}

#[derive(Debug, Default)]
struct McaiRustWorker {}

default_rust_mcai_worker_description!();

impl McaiWorker<McaiWorkerParameters, RustMcaiWorkerDescription> for McaiRustWorker {
  fn init(&mut self) -> Result<()> {
    info!("Initializing worker");
    Ok(())
  }

  fn process(
    &self,
    _channel: Option<McaiChannel>,
    _parameters: McaiWorkerParameters,
    job_result: JobResult,
  ) -> Result<JobResult> {
    info!("Processing the job with ID: {}", job_result.get_job_id());
    Ok(job_result)
  }

  // The following methods must be implemented for a Media Worker
  /* fn init_process(
    &mut self,
    parameters: MyWorkerParameters,
    _format_context: Arc<Mutex<FormatContext>>,
    _response_sender: Arc<Mutex<Sender<ProcessResult>>>,
  ) -> Result<Vec<StreamDescriptor>> {
    Ok(vec![StreamDescriptor::new_data(0)])
  }

  fn process_frames(
    &mut self,
    job_result: JobResult,
    stream_index: usize,
    frames: ProcessFrame,
  ) -> Result<ProcessResult> {
    Ok(ProcessResult::new_xml(something))
  }

  fn ending_process(&mut self) -> Result<()> {
    Ok(())
  } */
}

fn main() {
  let worker = McaiRustWorker::default();
  start_worker(worker);
}
